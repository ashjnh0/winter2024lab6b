public class GameManager {
	Deck drawPile;
	Card centerCard;
	Card playerCard;
	int cardsRemaining;

	public GameManager(){
		drawPile = new Deck();
		drawPile.shuffle();
		centerCard = drawPile.drawTopCard();
		playerCard = drawPile.drawTopCard();
		cardsRemaining = drawPile.length();
	}
	
	public String toString(){
		return "- - - - - - - - - - - - - - - - - - - -\n" + 
				"Center Card: " + centerCard + "\n" + 
				"Player Card: " + playerCard + "\n" +
				"- - - - - - - - - - - - - - - - - - - -";
	}

	public void dealCards(){
		if(drawPile.length() >= 1){
		drawPile.shuffle();
		centerCard = drawPile.drawTopCard();
		drawPile.shuffle();
		playerCard = drawPile.drawTopCard();
		cardsRemaining -= 2;
    }
  }

	public int getNumberOfCards(){
		return cardsRemaining;
	}

	public int calculatePoints(){
		if(playerCard.getRank() == centerCard.getRank()){
			return 4;
		} else if(playerCard.getSuit().equals(centerCard.getSuit())){
			return 2;
		} else {
			return -1;
		}
	}
  
}