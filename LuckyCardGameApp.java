//import java.util.Scanner;

public class LuckyCardGameApp {
	public static void main(String[] args){
		
	//Scanner scanner = new Scanner(System.in);
	GameManager manager = new GameManager();
	int totalPoints = 0;
	int round = 1;

	System.out.println("Welcome to the Lucky! card game :) \n Press enter to start the round");

	while(manager.getNumberOfCards() > 2 && totalPoints < 5){
		//scanner.nextLine();
		System.out.println("\nRound: " + round);
		System.out.println(manager);
		System.out.println("There are " + manager.getNumberOfCards() + " cards left");
		if (totalPoints >= 5 || manager.getNumberOfCards() < 1) {
			break;
	}

	totalPoints += manager.calculatePoints();
	System.out.println("Your point(s) : " + totalPoints + "\n");
	System.out.println("*  *  *  *  *  *  *  *  *  *  *  *  *  *");
	
	manager.dealCards();
	round++;
    }

	if(manager.getNumberOfCards() < 2 || totalPoints < 5) {
		System.out.println("Player loses, better luck next time! ");
	} else {
		System.out.println("Player wins with: " + totalPoints + " points");
    }
	//scanner.close();
  }
}