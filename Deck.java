import java.util.Random;

public class Deck {

    Card[] stack;
    Random rng;
    int length;

    public Deck() {
        rng = new Random();
        length = 52;
        int[] ranks = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        String[] suits = new String[]{"hearts", "spades", "clubs", "diamonds"};

        int i = 0;
        stack = new Card[length];

        for (int r : ranks) {
            for (String s : suits) {
                stack[i] = new Card(r, s); 
                i++;
            }
        }
    }

    public int length() {
        return this.stack.length; 
    }

    public Card drawTopCard() {
        if(length > 0){
			Card topCard = this.stack[length - 1];
			length--;
			return topCard;
		} else {
			return null;
		}
    }

    public void shuffle() {
        for (int i = 0; i < length - 1; i++) {
            int randomIndex = rng.nextInt(length - i) + i;
            Card t = stack[i];
            stack[i] = stack[randomIndex];
            stack[randomIndex] = t;
        }
    }

    public String toString() {
        String s = "";
        for (Card c : this.stack) { 
            s += c.toString() + "\n";
        }
        return s;
    }
}
