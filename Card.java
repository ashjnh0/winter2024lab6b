public class Card {
	
	private int rank;
	private String suit;
	
	public Card(int rank, String suit) {
		this.rank = rank;
		this.suit = suit;
	}
	
	public int getRank() {
		return this.rank;
	}
	
	public String getSuit() {
		return this.suit;
	}
	
	public String toString() {
		if (rank == 1) {
			return "Ace of " + suit;
		} else if (rank == 11) {
			return "Jack of " + suit;
		} else if (rank == 12) {
			return "Queen of " + suit;
		} else if (rank == 13) {
			return "King of " + suit;
		} else {
			return rank + " of " + suit;
		}
	}
}